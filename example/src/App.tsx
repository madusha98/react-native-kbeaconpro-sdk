import * as React from 'react';

import {
  StyleSheet,
  View,
  Text,
  Button,
  NativeEventEmitter,
  NativeModules,
  TouchableOpacity,
  Platform,
  ToastAndroid,
} from 'react-native';

import {
  clearBeacons,
  connectToDevice,
  disconnect,
  getCurrentBeacon,
  startScan,
  stopScan,
} from 'react-native-kbeaconpro-sdk';
import BackgroundService from 'react-native-background-actions';

export default function App() {
  const [result, setResult] = React.useState<string | undefined>();
  const [data, setData] = React.useState<any>([]);
  const [deviceState, setDeviceState] = React.useState('not connected');
  const eventEmitter = new NativeEventEmitter(NativeModules.KbeaconproSdk);

  const start = () => {
    startScan().then(setResult);
  };

  const stop = () => {
    stopScan();
  };

  const clear = async () => {
    try {
      await clearBeacons();
    } catch (error) {
      console.log(error);
    }
  };

  const sleep = (time: number) =>
    new Promise<void>((resolve) => setTimeout(() => resolve(), time));

  const runForever = async () => {
    return await new Promise<void>(async () => {
      for (let i = 0; BackgroundService.isRunning(); i++) {
        console.log(i);
        await sleep(10000);
      }
    });
  };

  const closeBackground = async () => {
    await BackgroundService.stop();
    ToastAndroid.show('Device Disconnected', ToastAndroid.SHORT);
  };

  const listenInBackground = async () => {
    const options = {
      taskName: 'Example',
      taskTitle: 'Bluetooth Device connected',
      taskDesc: 'Application is running in background',
      taskIcon: {
        name: 'ic_launcher',
        type: 'mipmap',
      },
      color: '#ff00ff',
      linkingURI: 'yourSchemeHere://chat/jane',
      parameters: {},
    };

    await BackgroundService.start(runForever, options);
    await BackgroundService.updateNotification({
      taskDesc: 'New ExampleTask description',
    }); // Only Android, iOS will ignore this call
    // iOS will also run everything here in the background until .stop() is called
  };

  React.useEffect(() => {
    getCurrentBeacon()
      .then((beacon) => {
        let temp = data;
        temp.push(beacon);
        setData(temp);
        setDeviceState('Connected');
        console.log(beacon);
      })
      .catch((err) => {
        console.log(err);
      });
    eventEmitter.removeAllListeners('onBeaconDiscovered');
    eventEmitter.removeAllListeners('onConnStateChange');
    eventEmitter.removeAllListeners('onNotifyDataReceived');
    eventEmitter.addListener('onBeaconDiscovered', (event) => {
      console.log(event.data);
      if (event.data) {
        setData(event.data);
      }
    });

    eventEmitter.addListener('onConnStateChange', async (event) => {
      console.log(event);
      if (event.state === 'Connected') {
        listenInBackground();
      } else if (event.state === 'Disconnected') {
        closeBackground();
      }
      setDeviceState(event.state);
    });
    eventEmitter.addListener('onNotifyDataReceived', (event) => {
      ToastAndroid.show(JSON.stringify(event), ToastAndroid.SHORT);
    });
  }, []);

  const renderItem = (item: any) => (
    <TouchableOpacity
      onPress={() => {
        const deviceID = Platform.OS === 'ios' ? item.uuidString : item.mac;
        if (!item.isConnected) {
          connectToDevice(deviceID, '0000000000000000', 10000);
        } else {
          disconnect(deviceID);
        }
      }}
      key={item}
    >
      <Text>{item.name}</Text>
      <Text>{item.isConnected ? 'Connected' : 'Not Connected'}</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <Button title="Start" onPress={start} />
      <Button title="Stop" onPress={stop} />
      <Button title="Clear" onPress={clear} />
      <Text>Result: {result}</Text>
      <Text>Device State: {deviceState}</Text>
      {data.map((item) => renderItem(item))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
