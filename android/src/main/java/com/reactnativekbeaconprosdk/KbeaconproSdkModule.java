package com.reactnativekbeaconprosdk;

import android.Manifest;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.WritableMap;

import com.kkmcn.kbeaconlib2.KBCfgPackage.KBCfgTrigger;
import com.kkmcn.kbeaconlib2.KBCfgPackage.KBTriggerAction;
import com.kkmcn.kbeaconlib2.KBCfgPackage.KBTriggerType;
import com.kkmcn.kbeaconlib2.KBConnState;
import com.kkmcn.kbeaconlib2.KBConnectionEvent;
import com.kkmcn.kbeaconlib2.KBException;
import com.kkmcn.kbeaconlib2.KBeacon;
import com.kkmcn.kbeaconlib2.KBeaconsMgr;


import java.util.ArrayList;
import java.util.HashMap;


@ReactModule(name = KbeaconproSdkModule.NAME)
public class KbeaconproSdkModule extends ReactContextBaseJavaModule implements  KBeaconsMgr.KBeaconMgrDelegate, KBeacon.NotifyDataDelegate {
    public static final String NAME = "KbeaconproSdk";
    private static String LOG_TAG = "KbeaconproSdkModule";

    private ReactApplicationContext context;
    private HashMap<String, KBeacon> mBeaconsDictory = new HashMap<>();
    private KBeacon[] mBeaconsArray;
    private KBeaconsMgr mBeaconsMgr;
    private KBConnState nDeviceLastState;
    private KBeacon mBeacon;

    private KBeacon.ConnStateDelegate connectionDelegate = new KBeacon.ConnStateDelegate() {
      @Override
      public void onConnStateChange(KBeacon beacon, KBConnState state, int nReason) {
        if (state == KBConnState.Connected)
        {
          Log.v(LOG_TAG, "device has connected");
          nDeviceLastState = state;
          enableButtonTriggerEvent2App();
          sendOnBeaconDiscoveredEvent(beacon);
        }
        else if (state == KBConnState.Connecting)
        {
          Log.v(LOG_TAG, "device start connecting");
          nDeviceLastState = state;
        }
        else if (state == KBConnState.Disconnecting)
        {
          Log.v(LOG_TAG, "device start disconnecting");
          nDeviceLastState = state;
        }
        else if (state == KBConnState.Disconnected)
        {
          if (nReason == KBConnectionEvent.ConnAuthFail) {
            Log.e(LOG_TAG, "Password Error");
          } else if (nReason == KBConnectionEvent.ConnTimeout) {
            Log.e(LOG_TAG, "Connection timeout");
          } else {
            Log.e(LOG_TAG, "connection error:" +  nReason);
          }

          nDeviceLastState = state;
          Log.e(LOG_TAG, "device has disconnected:" +  nReason);
        }

        WritableMap map = new WritableNativeMap();
        map.putString("state", state.toString());

        sendEvent(context, "onConnStateChange", map);
      }
    };


    public KbeaconproSdkModule(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    @ReactMethod
    public void startScan(int timeout, Promise promise) {
      mBeaconsMgr = KBeaconsMgr.sharedBeaconManager(context.getApplicationContext());
      if (mBeaconsMgr == null) {
        promise.resolve("Not Working");
      }
      //for android6, the app need corse location permission for BLE scanning
      if (ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(context.getCurrentActivity(),
          new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
      }
      //for android10, the app need fine location permission for BLE scanning
      if (ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(context.getCurrentActivity(),
          new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
      }
      mBeaconsMgr.delegate = this;
      int nStartScan = mBeaconsMgr.startScanning();
      if (nStartScan == 0) {
        Log.v(LOG_TAG, "start scan success");
      } else if (nStartScan == KBeaconsMgr.SCAN_ERROR_BLE_NOT_ENABLE) {
        promise.resolve("BLE function is not enable");
      } else if (nStartScan == KBeaconsMgr.SCAN_ERROR_NO_PERMISSION) {
        promise.resolve("BLE scanning has no location permission");
      } else {
        promise.resolve("BLE scanning unknown error");
      }

      promise.resolve("Working");

    if (timeout != 0){
      new java.util.Timer().schedule(
        new java.util.TimerTask() {
          @Override
          public void run() {
            stopScan();
          }
        },
        timeout
      );
    }
    }

    @ReactMethod
    public void stopScan(){
      mBeaconsMgr.stopScanning();
      Log.v(LOG_TAG, "stopped");
    }

    @ReactMethod
    public void clearBeacons(Promise promise){
      if (mBeaconsMgr != null){
        mBeaconsDictory.clear();
        mBeaconsArray = null;
        mBeaconsMgr.clearBeacons();

        WritableMap map = new WritableNativeMap();
        WritableArray array = new WritableNativeArray();
        map.putArray("data", array);
        sendEvent(context, "onBeaconDiscovered", map);

        Log.v(LOG_TAG, "cleared");
        promise.resolve("Cleared");
      } else {
        promise.reject(new Exception("mBeaconsMgr not initialized"));
      }
    }

    @ReactMethod
    public void connectToDevice(String beaconMac, String password, int timeout){
      KBeacon beacon = mBeaconsDictory.get(beaconMac);
      beacon.connect(password, timeout, connectionDelegate);
      mBeacon = beacon;
    }

  @ReactMethod
  public void disconnect(String beaconMac){
    KBeacon beacon = mBeaconsDictory.get(beaconMac);
    beacon.disconnect();
    sendOnBeaconDiscoveredEvent(beacon);
    mBeacon = null;
  }

  @ReactMethod
  public void getCurrentBeacon(Promise promise){
    if (mBeacon != null){
      WritableMap map = new WritableNativeMap();
      map.putString("mac", mBeacon.getMac());
      map.putString("name", mBeacon.getName());
      map.putInt("batteryPercent", mBeacon.getBatteryPercent());
      map.putInt("rssi", mBeacon.getRssi());
      map.putBoolean("isConnected", mBeacon.getState() == KBConnState.Connected);

      promise.resolve(map);
    } else {
      promise.reject(new Exception("Not connected to any devices"));
    }
  }



  @Override
  public void onNotifyDataReceived(KBeacon beacon, int nEventType, byte[] sensorData) {
    WritableMap map = new WritableNativeMap();
    map.putInt("event", nEventType);

    sendEvent(context, "onNotifyDataReceived", map);
  }

  @Override
  public void onBeaconDiscovered(KBeacon[] beacons)
  {
    for (KBeacon pBeacons: beacons)
    {
      mBeaconsDictory.put(pBeacons.getMac(), pBeacons);
      Log.v(LOG_TAG, pBeacons.getName());

    }

    if (mBeacon != null){
      if (mBeacon.isConnected() && mBeaconsDictory.get(mBeacon.getMac()) != null){
        mBeaconsDictory.put(mBeacon.getMac(), mBeacon);
      }
    }

    if (mBeaconsDictory.size() > 0) {
      mBeaconsArray = new KBeacon[mBeaconsDictory.size()];
      mBeaconsDictory.values().toArray(mBeaconsArray);
    }

    Log.v(LOG_TAG, mBeaconsDictory.toString());
    sendOnBeaconDiscoveredEvent(null);

  }

  public void sendOnBeaconDiscoveredEvent(KBeacon beacon){
    if (beacon != null){
      if (mBeacon.isConnected() && mBeaconsDictory.get(mBeacon.getMac()) != null){
        mBeaconsDictory.put(mBeacon.getMac(), mBeacon);
        mBeaconsArray = new KBeacon[mBeaconsDictory.size()];
        mBeaconsDictory.values().toArray(mBeaconsArray);

      }
    }
    WritableArray array = new WritableNativeArray();
    for (KBeacon kBeacon: mBeaconsArray) {
      WritableMap map = new WritableNativeMap();
      map.putString("mac", kBeacon.getMac());
      map.putString("name", kBeacon.getName());
      map.putInt("batteryPercent", kBeacon.getBatteryPercent());
      map.putInt("rssi", kBeacon.getRssi());
      map.putBoolean("isConnected", kBeacon.getState() == KBConnState.Connected);
      array.pushMap(map);
    }
    WritableMap map = new WritableNativeMap();
    map.putArray("data", array);

    sendEvent(context, "onBeaconDiscovered", map);


  }

  @Override
  public void onCentralBleStateChang(int nNewState) {

  }

  @Override
  public void onScanFailed(int errorCode) {

  }

  public void enableButtonTriggerEvent2App() {

    if (!mBeacon.isConnected()) {
      Log.v(LOG_TAG,"Device is not connected");
      return;
    }

    //check device capability
    final int nTriggerType = KBTriggerType.BtnSingleClick;

    //set trigger type
    KBCfgTrigger btnTriggerPara = new KBCfgTrigger(0,
      nTriggerType);
    btnTriggerPara.setTriggerAction(KBTriggerAction.Report2App);

    //enable push button trigger

    this.mBeacon.modifyConfig(btnTriggerPara, new KBeacon.ActionCallback() {
      public void onActionComplete(boolean bConfigSuccess, KBException error) {

        if (bConfigSuccess) {
          //subscribe humidity notify
          if (!mBeacon.isSensorDataSubscribe(nTriggerType)) {
            mBeacon.subscribeSensorDataNotify(nTriggerType, KbeaconproSdkModule.this, new KBeacon.ActionCallback() {
              @Override
              public void onActionComplete(boolean bConfigSuccess, KBException error) {
                if (bConfigSuccess) {
                  Log.v(LOG_TAG,"subscribe button trigger event success");
                } else {
                  Log.v(LOG_TAG,"subscribe button trigger event failed");
                }
                enableButtonTriggerEvent2AppDoubleClick();
              }
            });
          }
        } else {
          Log.v(LOG_TAG,"enable push button trigger error:" + error.errorCode);
        }
      }
    });
  }

  public void enableButtonTriggerEvent2AppDoubleClick() {
    if (!mBeacon.isConnected()) {
      Log.v(LOG_TAG,"Device is not connected");
      return;
    }

    //check device capability
    final int nTriggerType = KBTriggerType.BtnDoubleClick;

    //set trigger type
    KBCfgTrigger btnTriggerPara = new KBCfgTrigger(1,
      nTriggerType);
    btnTriggerPara.setTriggerAction(KBTriggerAction.Report2App);

    //enable push button trigger

    this.mBeacon.modifyConfig(btnTriggerPara, new KBeacon.ActionCallback() {
      public void onActionComplete(boolean bConfigSuccess, KBException error) {

        if (bConfigSuccess) {
          //subscribe humidity notify
          if (!mBeacon.isSensorDataSubscribe(nTriggerType)) {
            mBeacon.subscribeSensorDataNotify(nTriggerType, KbeaconproSdkModule.this, new KBeacon.ActionCallback() {
              @Override
              public void onActionComplete(boolean bConfigSuccess, KBException error) {
                if (bConfigSuccess) {
                  Log.v(LOG_TAG,"subscribe button trigger event success");
                } else {
                  Log.v(LOG_TAG,"subscribe button trigger event failed");
                }
              }
            });
          }
        } else {
          Log.v(LOG_TAG,"enable push button trigger error:" + error.errorCode);
        }
      }
    });
  }

  private void sendEvent(ReactContext reactContext,
                         String eventName,
                         @Nullable WritableMap params) {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(eventName, params);
  }
}
