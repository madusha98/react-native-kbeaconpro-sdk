@objc(KbeaconproSdk)
class KbeaconproSdk: RCTEventEmitter, KBeaconMgrDelegate, ConnStateDelegate, NotifyDataDelegate {
    
    var mBeaconsDictory = [String:KBeacon]()
    
    var mBeaconsMgr: KBeaconsMgr?
    
    private var mSelectedBeacon : KBeacon?
    
    var mBeaconsArray = [KBeacon]()
    
    var nDeviceLastState: KBConnState = KBConnState.Disconnected
    
    
    override init() {
        super.init()
        mBeaconsMgr = KBeaconsMgr.sharedBeaconManager
        mBeaconsMgr!.delegate = self
    }
    
    @objc override static func requiresMainQueueSetup() -> Bool {
        return false
    }
    
    override func supportedEvents() -> [String]! {
        ["onBeaconDiscovered", "onConnStateChange", "onNotifyDataReceived"]
    }
    
    @objc(startScan:withResolver:withRejecter:)
    func startScan(_ timeout:Int, withResolver resolve:RCTPromiseResolveBlock, withRejecter reject:RCTPromiseRejectBlock) -> Void {
        let scanResult = mBeaconsMgr!.startScanning()
        if (scanResult)
        {
            NSLog("start scan success");
            resolve("start scan success")
            if (timeout != 0){
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(timeout)) {
                    self.mBeaconsMgr!.stopScanning()
                }
            }
        }
        else
        {
            NSLog("start scan failed");
        }
        
        
    }
    
    @objc(stopScan:withRejecter:)
    func stopScan(_ resolve:RCTPromiseResolveBlock, withRejecter reject:RCTPromiseRejectBlock) -> Void {
        mBeaconsMgr!.stopScanning()
        resolve("stop scan success")
    }
    
    @objc(clearBeacons:withRejecter:)
    func clearBeacons(_ resolve:RCTPromiseResolveBlock, withRejecter reject:RCTPromiseRejectBlock) -> Void {
        if (mBeaconsMgr != nil){
            mBeaconsDictory.removeAll()
            mBeaconsArray.removeAll()
            mBeaconsMgr?.clearBeacons()
            sendEvent(eventName: "onBeaconDiscovered", params: [
                        "data": mBeaconsArray.map({ (kb) -> Any in
                return [
                    "name": kb.name ?? "",
                    "mac": kb.mac ?? "",
                    "uuidString": kb.uuidString ?? "",
                    "batteryPercent": kb.batteryPercent,
                    "rssi": kb.rssi,
                    "isConnected": kb.isConnected()
                ]
            })])
            resolve("Cleared")
        } else {
            reject("failure", "mBeaconsMgr not initialized", nil)
        }
    }
    
    @objc(connectToDevice:withPassword:withTimeout:)
    func connectToDevice(_ uuid: String, password: String, timeout: Double) -> Void {
        print(uuid)
        if mBeaconsDictory[uuid] != nil {
            let kbeacon:KBeacon = mBeaconsDictory[uuid]!
            kbeacon.connect(password, timeout: timeout, delegate: self)
            mSelectedBeacon = kbeacon
            
            
        }
    }
    
    @objc(disconnect:)
    func disconnect(_ uuid: String ) -> Void {
        print(uuid)
        if mBeaconsDictory[uuid] != nil {
            let kbeacon:KBeacon = mBeaconsDictory[uuid]!
            kbeacon.disconnect()
            sendOnBeaconDiscoveredEvent(beacon: kbeacon)
            mSelectedBeacon = nil
        }
    }
    
    @objc(getCurrentBeacon:withRejecter:)
    func getCurrentBeacon(_ resolve:RCTPromiseResolveBlock, withRejecter reject:RCTPromiseRejectBlock) -> Void {
        if (mSelectedBeacon != nil){
            resolve([
                "name": mSelectedBeacon!.name ?? "",
                "mac": mSelectedBeacon!.mac ?? "",
                "uuidString": mSelectedBeacon!.uuidString ?? "",
                "batteryPercent": mSelectedBeacon!.batteryPercent,
                "rssi": mSelectedBeacon!.rssi,
                "isConnected": mSelectedBeacon!.isConnected()
            ])
        } else {
            reject("failure", "Not connected to any devcies", nil)
        }
        
    }
    
    
    
    func onBeaconDiscovered(beacons:[KBeacon])
    {
        for beacon in beacons
        {
            if mBeaconsDictory[beacon.uuidString!] == nil
            {
                mBeaconsArray.append(beacon)
            }
            mBeaconsDictory[beacon.uuidString!] = beacon
            
        }
        
        if mSelectedBeacon != nil {
            if mSelectedBeacon!.isConnected() && mBeaconsDictory[mSelectedBeacon!.uuidString!] == nil {
                mBeaconsArray.append(mSelectedBeacon!)
                mBeaconsDictory[mSelectedBeacon!.uuidString!] = mSelectedBeacon!
            }
        }
        
        self.sendOnBeaconDiscoveredEvent(beacon: nil)
        
    }
    
    func sendOnBeaconDiscoveredEvent(beacon:KBeacon?) {
        
        if beacon != nil {
            if beacon!.isConnected() && mBeaconsDictory[beacon!.uuidString!] == nil {
                mBeaconsArray.append(beacon!)
                mBeaconsDictory[beacon!.uuidString!] = beacon!
            }
        }
        
        sendEvent(eventName: "onBeaconDiscovered", params: [
                    "data": mBeaconsArray.map({ (kb) -> Any in
            return [
                "name": kb.name ?? "",
                "mac": kb.mac ?? "",
                "uuidString": kb.uuidString ?? "",
                "batteryPercent": kb.batteryPercent,
                "rssi": kb.rssi,
                "isConnected": kb.isConnected()
            ]
        })])
    }
    
    func onCentralBleStateChange(newState:BLECentralMgrState)
    {
        if (newState == BLECentralMgrState.PowerOn)
        {
            //the app can start scan in this case
            NSLog("central ble state power on")
        }
    }
    
    @objc func sendEvent( eventName: String, params: Any) {
        self.sendEvent(withName: eventName, body: params)
    }
    
    func onConnStateChange(_ beacon: KBeacon, state: KBConnState, evt: KBConnEvtReason) {
        var stateString = ""
        if (state == KBConnState.Connected){
            print("Device has connected")
            nDeviceLastState = state
            stateString = "Connected"
            self.onEnableButtonTriggerEvent2App()
            sendOnBeaconDiscoveredEvent(beacon: beacon)
        } else if (state == KBConnState.Connecting){
            print("Device is connecting")
            nDeviceLastState = state
            stateString = "Connecting"
        } else if (state == KBConnState.Disconnecting){
            print("Device is disconnecting")
            nDeviceLastState = state
            stateString = "Disconnecting"
        } else if (state == KBConnState.Disconnected){
            print("Device has disconnected")
            nDeviceLastState = state
            stateString = "Disconnected"
        }
        
        sendEvent(eventName: "onConnStateChange", params: ["state": stateString])
    }
    
    func onEnableButtonTriggerEvent2App()
    {
        guard self.mSelectedBeacon!.isConnected(),
            let commCfg = self.mSelectedBeacon!.getCommonCfg(),
            (commCfg.isSupportTrigger(KBTriggerType.BtnSingleClick)) else
        {
            print("not allowed to enable button trigger")
            return
        }

        //enable button trigger
        let btnTrigger = KBCfgTrigger(0, triggerType: KBTriggerType.BtnSingleClick)
        btnTrigger.setTriggerAction(KBTriggerAction.ReportToApp)
        self.mSelectedBeacon!.modifyConfig(obj: btnTrigger) { (result, exception) in
            if (result)
            {
                print("Enable button trigger to app")
                
                //subscribe button notification
                self.mSelectedBeacon!.subscribeSensorDataNotify(KBTriggerType.BtnSingleClick, notifyDelegate: self) { (result, exception) in
                    if (result){
                        print("subscribe button trigger notification success")
                    }else{
                        print("subscribe button trigger notification failed")
                    }
                    self.onEnableButtonTriggerEvent2AppDoubleClick()
                }
            }
        }
    }
    
    func onEnableButtonTriggerEvent2AppDoubleClick()
    {
        guard self.mSelectedBeacon!.isConnected(),
            let commCfg = self.mSelectedBeacon!.getCommonCfg(),
            (commCfg.isSupportTrigger(KBTriggerType.BtnDoubleClick)) else
        {
            print("not allowed to enable button trigger")
            return
        }

        //enable button trigger
        let btnTrigger = KBCfgTrigger(1, triggerType: KBTriggerType.BtnDoubleClick)
        btnTrigger.setTriggerAction(KBTriggerAction.ReportToApp)
        self.mSelectedBeacon!.modifyConfig(obj: btnTrigger) { (result, exception) in
            if (result)
            {
                print("Enable button trigger to app")
                
                //subscribe button notification
                self.mSelectedBeacon!.subscribeSensorDataNotify(KBTriggerType.BtnDoubleClick, notifyDelegate: self) { (result, exception) in
                    if (result){
                        print("subscribe button trigger notification success")
                    }else{
                        print("subscribe button trigger notification failed")
                    }
                }
            }
        }
    }
    
    func enableBtnTriggerEvtToAppDoubleClick()
    {
        //check if device can support button trigger capibility
        if let commCfg = self.mSelectedBeacon!.getCommonCfg(),
           !(commCfg.isSupportTrigger(KBTriggerType.BtnDoubleClick))
        {
            print("device does not support button trigger")
            return
        }

        //trigger index is 0
        let btnTriggerPara = KBCfgTrigger(0, triggerType: KBTriggerType.BtnDoubleClick)
        //set trigger action to app
        btnTriggerPara.setTriggerAction(KBTriggerAction.ReportToApp)

        self.mSelectedBeacon!.modifyConfig(obj: btnTriggerPara) { (result, exception) in
            if (result)
            {
                print("config trigger success double click")
                print(result)
            }
            else
            {
                print("config trigger failed double click");
                print(exception?.errorDescription)
            }
        }
    }
    
    func onNotifyDataReceived(_ beacon: KBeacon, evt: Int, data: Data) {
        print("recieve event:\(evt), content:\(data.count)")
        sendEvent(eventName: "onNotifyDataReceived", params: ["event": evt])
    }
    

    

}
