#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(KbeaconproSdk, NSObject)

RCT_EXTERN_METHOD(startScan:(int)timeout
                  withResolver:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(stopScan:(RCTPromiseResolveBlock)resolve
                 withRejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(clearBeacons:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(connectToDevice:(NSString)uuid
                  withPassword:(NSString)password
                  withTimeout:(double)timeout)

RCT_EXTERN_METHOD(disconnect:(NSString)uuid)

RCT_EXTERN_METHOD(getCurrentBeacon:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject)


@end
