import { NativeModules, Platform } from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-kbeaconpro-sdk' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const KbeaconproSdk = NativeModules.KbeaconproSdk
  ? NativeModules.KbeaconproSdk
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export function startScan(timeout: number = 0): Promise<string> {
  return KbeaconproSdk.startScan(timeout);
}

export function stopScan() {
  return KbeaconproSdk.stopScan();
}

export function clearBeacons() {
  return KbeaconproSdk.clearBeacons();
}

export function connectToDevice(
  beaconMac: string,
  password: string,
  timeout: number
) {
  return KbeaconproSdk.connectToDevice(beaconMac, password, timeout);
}

export function disconnect(beaconMac: string) {
  return KbeaconproSdk.disconnect(beaconMac);
}

export function getCurrentBeacon(): Promise<any> {
  return KbeaconproSdk.getCurrentBeacon();
}
